var url = require('url');
var request = require("request");
var express = require('express');
var bodyParser = require('body-parser');
var firebase = require("firebase");
// Initialize Firebase
var config = {
    apiKey: "AIzaSyCzlEpgvT3IUvTxTs7pJUD81CFYvAXwOzU",
    authDomain: "cde-assignment-part2.firebaseapp.com",
    databaseURL: "https://cde-assignment-part2.firebaseio.com",
    projectId: "cde-assignment-part2",
    storageBucket: "",
    messagingSenderId: "657682977144"
};
firebase.initializeApp(config);

var server_port = process.env.PORT || 8080;
var app = express();
app.use(bodyParser.json())

app.use(function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader("Access-Control-Allow-Headers","content-type");  
    res.setHeader('Access-Control-Allow-Credentials',true);
    if(req.method=="OPTIONS") res.send(200);
    else  next();
});

//redirect to getWeather
app.get('/', function(req, res, next){
   console.log('root URL!');
   res.header('Location','/getWeather');
   res.sendStatus(302);
   res.end();
});

//get weather
app.get('/getWeather',function(req, res, next){
    console.log("\n***Start get weather***");
    var cityName = "Hong Kong";//set "Hong Kong" as defalut

    //get the city name from url
    var params = url.parse(req.url, true).query;
    if(params.city != null ){
        cityName = params.city;
    }

    var reslut = new Promise(function(resolve, reject){
        var json = getWeatherJSON(cityName);
        //set time out 0.5 second
        setTimeout(function(){
            console.log(json);
            
            //check the return json is null
            if(json.city != null){
                resolve({
                   success:true,
                   result:[json]
                });
            }else{
                resolve({
                   success:false,
                   message:"Invaild city name"
                });
            }
            console.log("***End of get weather***");
        },200);
    });
    
    reslut.then(function(value){
        //send the json to body
        res.send(value);
        res.end();
    });
});

app.post('/addToBookmark',function(req, res, next){
    console.log('\n***Start Add a City To Bookmark***');
   
    var bodyJSON = req.body;
    var reslut = new Promise(function(resolve, reject){
        var json;
        var link = firebase.database().ref().child('city');
        var data;//store the json from firebase
        link.once('value').then(function(snapshot){//request the json form firebase
            data = snapshot.val();
        });
        if(bodyJSON.city != undefined){
            json = getWeatherJSON(bodyJSON.city);
        }
        //set time out 1 second for get bookmarked city name from firebase, and get the weather from openweather
        setTimeout(function(){
            var chkName = true;
            if(json.city == undefined){
                chkName = false;
            }
            if(chkName){
                var isDuplicate = false;
                for(var key in data){
                    if(data[key].city == bodyJSON.city && data[key].email == bodyJSON.email){
                        isDuplicate = true;
                        break;
                    }
                    if(isDuplicate){
                        break;
                    }
                }
                if(isDuplicate){
                    resolve({
                        success:false,
                        message:"The city name exist"
                    });
                }else{
                    var ref = firebase.database().ref('/city');
                    var record = {};
                    record.city = bodyJSON.city;
                    record.email = bodyJSON.email;
                    ref.push(record);
                    console.log("Added city:"+bodyJSON.city);
                    resolve({
                        success:true,
                        message:"The city name added to bookmark"
                    });
                }
            }else{
                resolve({
                    success:false,
                    message:"Invaild city name"
                });
            }
            console.log('***End of Add a City To Bookmark***');
        },400);
    });
   
    reslut.then(function(value){
        //send the json to body
        res.send(value);
        res.end();
    });
});

//get the bookmark list
app.get('/getBookmarkList',function(req, res, next){
    console.log('\n***Start Get Bookmark List***');
    var params = url.parse(req.url, true).query;
    var email = params.email;
    console.log(email);
    var reslut = new Promise(function(resolve, reject){
        var array = [];//store json from open weather api
        var link = firebase.database().ref().child('city');
        var data;//store the json from firebase
        link.once('value').then(function(snapshot){//request the json form firebase
            data = snapshot.val();
            for(var key in data){//get the id form data
                console.log("firebase:" + data[key].email + "client:" + email);
                if(data[key].email == email){
                    console.log(data[key]);
                    array.push(getWeatherJSON(data[key].city));//store a city weather information to array
                }
            }
        });

        //set time out 2 second for get bookmarked city name from firebase, and get the weather from openweather
        setTimeout(function(){
            //check the return json is null
            if(array.length > 0){
                resolve({
                    success:true,
                    result:array
                });
                console.log(array);
            }else{
                resolve({
                    success:false,
                    message:"The bookmark list is empty"
                });
            }
            console.log('***End of Get Bookmark List***');
        },1000);
    });
    
    reslut.then(function(value){
        //send the json to body
        res.send(value);
        res.end();
    });
});

app.put('/updateToBookmark',function(req, res, next){
    console.log('\n***Start Update a City to Bookmark***');
   
    var reslut = new Promise(function(resolve, reject){
        var bodyJSON = req.body;
        console.log(bodyJSON);
        var link = firebase.database().ref().child('city');
        var data;//store the json from firebase
        link.once('value').then(function(snapshot){//request the json form firebase
            data = snapshot.val();
        });
        var json;
        if(bodyJSON.city_old != null && bodyJSON.city_new != null){
            json = getWeatherJSON(bodyJSON.city_new);
        }
        
        //set time out 1 second for get bookmarked city name from firebase, and get the weather from openweather
        setTimeout(function(){
            var chkName = true;
            if(json.city == undefined){
                chkName = false;
            }
            if(chkName){
                var oldCityName = bodyJSON.city_old;
                var newCityName = bodyJSON.city_new;
                var email = bodyJSON.email;
                var oid = "";
                var nid = "";
                
                for(var key in data){
                    if(data[key].city == oldCityName && data[key].email == email){
                        oid = key;
                    }
                    if(data[key].city == newCityName && data[key].email == email){
                        nid = key;
                    }
                }
                
                if(oid == ""){
                    resolve({
                        success:false,
                        message:"The old city name does not exist"
                    });
                }else if(nid != ""){
                    resolve({
                        success:false,
                        message:"The new city name already exist"
                    });
                }else{
                    var ref = firebase.database().ref('/city');
                    var cityObj = {}
                    cityObj['/'+oid+'/city'] = newCityName;
                    cityObj['/'+oid+'/email'] = email;
                    ref.update(cityObj);
                    console.log("Change city name '"+oldCityName+"' to '"+newCityName+"'");
                    resolve({
                        success:true,
                        message:"The city name updated"
                    });
                }
            }else{
                resolve({
                    success:false,
                    message:"Invaild new city name"
                });
            }
            console.log('***End of Update a City to Bookmark***');
        },1000);
    });
   
    reslut.then(function(value){
        //send the json to body
        res.send(value);
        res.end();
    });
});

app.delete('/delFromBookmark',function(req, res, next){
   console.log('\n***Start Delete a City from Bookmark***');
   
    var cityName = "";
    var email = "";
    var params = url.parse(req.url, true).query;
   
    var reslut = new Promise(function(resolve, reject){
        var link = firebase.database().ref().child('city');
        var data;//store the json from firebase
        link.once('value').then(function(snapshot){//request the json form firebase
            data = snapshot.val();
        });

        //set time out 2 second for get bookmarked city name from firebase, and get the weather from openweather
        setTimeout(function(){
            
            if(params.city != null && params.email != null){
                cityName = params.city;
                email = params.email;
                var id = "";
                
                for(var key in data){
                    if(data[key].city == cityName && data[key].email == email){
                        id = key;
                        break;
                    }
                }
                if(id == ""){
                    resolve({
                        success:false,
                        message:"The city name does not exist"
                    });
                }else{
                    var ref = firebase.database().ref('/city');
                    ref.child(id).remove();
                    console.log("delete city:"+cityName);
                    resolve({
                        success:true,
                        message:"The city name deleted"
                    });
                }
            }else{
                resolve({
                    success:false,
                    message:"plase add '?city=city_name_to_be_delete' after url"
                });
            }
            console.log('***End of Delete a City from Bookmark***');
        },1000);
    });
   
    reslut.then(function(value){
        //send the json to body
        res.send(value);
        res.end();
    });
});

//download the json from open weather api
function getWeatherJSON(cityName){
    
    //open weather api url
    var apiURL = "http://api.openweathermap.org/data/2.5/weather?q="+cityName+"&appid=7bd53a9772cce2586ccfd912acca895f";
    
    //download the json from open weather
    var json = {};
    request(apiURL, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            if(data.name != null){
                json.city = data.name;
                json.country = data.sys.country;
                json.weatherMain = data.weather[0].main;
                json.weatherDescription = data.weather[0].description;
                json.temperature = (data.main.temp-273.15).toFixed(2);
                json.temperature_max = (data.main.temp_max-273.15).toFixed(2);
                json.temperature_min = (data.main.temp_min-273.15).toFixed(2);
                json.humidity = data.main.humidity;
            }else{
                json = data;
            }
        }

    });
    
    return json;
}

var server = app.listen(server_port, function () {
  var host = "0.0.0.0";
  var port = server.address().port;
  console.log("Server Start at http://%s:%s", host, port)
});
